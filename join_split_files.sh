#!/bin/bash

cat vendor/lib64/librelight_only.so.* 2>/dev/null >> vendor/lib64/librelight_only.so
rm -f vendor/lib64/librelight_only.so.* 2>/dev/null
cat vendor/app/MiGameService/MiGameService.apk.* 2>/dev/null >> vendor/app/MiGameService/MiGameService.apk
rm -f vendor/app/MiGameService/MiGameService.apk.* 2>/dev/null
cat vendor/data-app/SmartHome/SmartHome.apk.* 2>/dev/null >> vendor/data-app/SmartHome/SmartHome.apk
rm -f vendor/data-app/SmartHome/SmartHome.apk.* 2>/dev/null
cat system/system/lib64/librelight_only_extraphoto.so.* 2>/dev/null >> system/system/lib64/librelight_only_extraphoto.so
rm -f system/system/lib64/librelight_only_extraphoto.so.* 2>/dev/null
cat system/system/apex/com.android.vndk.current.apex.* 2>/dev/null >> system/system/apex/com.android.vndk.current.apex
rm -f system/system/apex/com.android.vndk.current.apex.* 2>/dev/null
cat system/system/apex/com.android.art.release.apex.* 2>/dev/null >> system/system/apex/com.android.art.release.apex
rm -f system/system/apex/com.android.art.release.apex.* 2>/dev/null
cat system/system/app/VoiceAssist/VoiceAssist.apk.* 2>/dev/null >> system/system/app/VoiceAssist/VoiceAssist.apk
rm -f system/system/app/VoiceAssist/VoiceAssist.apk.* 2>/dev/null
cat system/system/priv-app/Music/Music.apk.* 2>/dev/null >> system/system/priv-app/Music/Music.apk
rm -f system/system/priv-app/Music/Music.apk.* 2>/dev/null
cat system/system/priv-app/SecurityCenter/SecurityCenter.apk.* 2>/dev/null >> system/system/priv-app/SecurityCenter/SecurityCenter.apk
rm -f system/system/priv-app/SecurityCenter/SecurityCenter.apk.* 2>/dev/null
cat system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> system/system/priv-app/MiuiCamera/MiuiCamera.apk
rm -f system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat system/system/priv-app/MiuiVideo/MiuiVideo.apk.* 2>/dev/null >> system/system/priv-app/MiuiVideo/MiuiVideo.apk
rm -f system/system/priv-app/MiuiVideo/MiuiVideo.apk.* 2>/dev/null
cat system/system/priv-app/Browser/Browser.apk.* 2>/dev/null >> system/system/priv-app/Browser/Browser.apk
rm -f system/system/priv-app/Browser/Browser.apk.* 2>/dev/null
cat system/system/data-app/com.autonavi.minimap_12/com.autonavi.minimap_12.apk.* 2>/dev/null >> system/system/data-app/com.autonavi.minimap_12/com.autonavi.minimap_12.apk
rm -f system/system/data-app/com.autonavi.minimap_12/com.autonavi.minimap_12.apk.* 2>/dev/null
cat system/system/data-app/Youpin/Youpin.apk.* 2>/dev/null >> system/system/data-app/Youpin/Youpin.apk
rm -f system/system/data-app/Youpin/Youpin.apk.* 2>/dev/null
cat system/system/data-app/com.taobao.taobao_24/com.taobao.taobao_24.apk.* 2>/dev/null >> system/system/data-app/com.taobao.taobao_24/com.taobao.taobao_24.apk
rm -f system/system/data-app/com.taobao.taobao_24/com.taobao.taobao_24.apk.* 2>/dev/null
cat system/system/data-app/MiShop/MiShop.apk.* 2>/dev/null >> system/system/data-app/MiShop/MiShop.apk
rm -f system/system/data-app/MiShop/MiShop.apk.* 2>/dev/null
cat system/system/data-app/MiFinance/MiFinance.apk.* 2>/dev/null >> system/system/data-app/MiFinance/MiFinance.apk
rm -f system/system/data-app/MiFinance/MiFinance.apk.* 2>/dev/null
cat system/system/data-app/VipAccount/VipAccount.apk.* 2>/dev/null >> system/system/data-app/VipAccount/VipAccount.apk
rm -f system/system/data-app/VipAccount/VipAccount.apk.* 2>/dev/null
cat vendor_bootimg/09_dtbdump_t7'd^bsR.dtb.* 2>/dev/null >> vendor_bootimg/09_dtbdump_t7'd^bsR.dtb
rm -f vendor_bootimg/09_dtbdump_t7'd^bsR.dtb.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
