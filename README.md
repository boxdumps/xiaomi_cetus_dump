## missi-user 11 RKQ1.201112.002 21.9.27 release-keys
- Manufacturer: xiaomi
- Platform: lahaina
- Codename: cetus
- Brand: Xiaomi
- Flavor: missi-user
- Release Version: 11
- Id: RKQ1.201112.002
- Incremental: 21.9.27
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: 560
- Fingerprint: Xiaomi/cetus/cetus:11/RKQ1.201112.002/21.9.27:user/release-keys
- OTA version: 
- Branch: missi-user-11-RKQ1.201112.002-21.9.27-release-keys
- Repo: xiaomi_cetus_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
